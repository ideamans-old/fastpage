<?php

/**
 * FastPage APC plugin
 *
 * Licensed under the MIT License
 *
 * @copyright Copyright 2011, ideaman's Inc. (http://www.ideamans.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Version.
define( 'FASTPAGE_APC_VERSION', '1.0.1' );

/**
 * APC cache helper.
 *
 * @package FastPage
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 * @var string $key_glue Glue string to combile namespace and key.
 */
class FastPage_APC extends FastPage_Cache {
  /**
   * Constructor.
   */
  public function __construct( $fastpage ) {
    parent::__construct($fastpage);
    $this->version = FASTPAGE_APC_VERSION;

    $config = $fastpage->config('apc');

    if ( !function_exists('apc_store') ) {
      throw new FastPage_Exception( 'APC is not enabled.' );
    }

    $this->key_glue = $config->key_glue;
    if ( is_null($this->key_glue) ) $this->key_glue = '\\';
  }

  /**
   * Combine namespace and key.
   *
   * @param string $namespace Namespace.
   * @param string $raw_key Raw key name.
   * @return string Combined key name.
   */
  public function combine_key( $namespace, $raw_key ) {
    $key = $this->normalize_key($raw_key);
    return $namespace . $this->key_glue . $key;
  }

  /**
   * Override exist method.
   */
  public function exist( $namespace, $raw_key ) {
    $exist = apc_exists( $this->combine_key( $namespace, $raw_key ) );
    if ( $exist ) {
      // FIXME: Check value, because APC looks like not to see ttl in checking exists.
      $value = $this->get( $namespace, $raw_key );
      if ( !isset($value) ) $exist = false;
    }
    return $exist;
  }

  /**
   * Override get method.
   */
  public function get( $namespace, $raw_key ) {
    $value = apc_fetch( $this->combine_key( $namespace, $raw_key ) );
    if ( false === $value ) return null;
    return $value;
  }

  /**
   * Override set method.
   */
  public function set( $namespace, $raw_key, $value, $expire = null ) {
    // FIXME: APC expiratin looks like not to work.
    if ( !isset($expire) ) $expire = $this->default_expire;
    apc_store( $this->combine_key( $namespace, $raw_key ), $value, $expire );
  }

  /**
   * Override delete method.
   */
  public function delete( $namespace, $raw_key ) {
    apc_delete( $this->combine_key( $namespace, $raw_key ) );
  }

  /**
   * Override purge method.
   */
  public function purge( $namespace = null ) {
    // APC will purge automatically.
    return;
  }

  /**
   * Override flush method.
   */
  public function flush( $namespace = null ) {
    // APC not supports namespace flush.
    apc_clear_cache('user');
  }
}

/**
 * APC plugin.
 *
 * @package FastPage
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class FastPage_Plugin_APC extends FastPage_Plugin {
  /**
   * Plugin constructor.
   *
   * @param FastPage $fastpage FastPage instance.
   */
  public function __construct( $fastpage ) {
    parent::__construct( $fastpage );
    $this->version = FASTPAGE_APC_VERSION;

    // If APC enabled, change cache helper.
    if ( function_exists('apc_store') ) {
      $fastpage->helper_class( 'cache', 'FastPage_APC' );
    }
  }
}
