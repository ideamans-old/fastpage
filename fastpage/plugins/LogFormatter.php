<?php

/**
 * FastPage LogFormatter plugin
 *
 * Licensed under the MIT License
 *
 * @copyright Copyright 2011, ideaman's Inc. (http://www.ideamans.com)
 * @license MIT License (http://www.opensource.org/licenses/mit-license.php)
 */

// Version.
define( 'FASTPAGE_LOG_FORMATTER_VERSION', '1.0.0' );

/**
 * Debug log report.
 *
 * @package FastPage_LogFormatter
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 * @var array $sections Sections.
 */
class FastPage_LogFormatter_Report extends FastPage_Object {
  public $sections = array();

  /**
   * Add a new section.
   *
   * @param $title Title of the section.
   * @return FastPage_LogFormatter_ReportSection A new section object.
   */
  public function add_section($title) {
    $section = new FastPage_LogFormatter_ReportSection;

    $section->title = $title;
    $section->report = $this;

    $this->sections[$title] = $section;
    return $section;
  }
}

/**
 * Debug log report section.
 *
 * @package FastPage_LogFormatter
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 * @var array $columns Columns.
 * @var array $rows Rows.
 */
class FastPage_LogFormatter_ReportSection extends FastPage_Object {
  public $columns = array();
  public $rows = array();

  /**
   * Add a new column.
   *
   * @param string $name Reporting label.
   * @param string $prop Property name.
   * @param string $format Format type: integer, float or string.
   * @param integer $decimal Decimals for float format.
   * @return FastPage_LogFormatter_ReportColumn A new column object.
   */
  public function add_column( $name, $prop, $format = null, $decimal = 2 ) {
    $column = new FastPage_LogFormatter_ReportColumn;

    $column->name = $name;
    $column->prop = $prop;
    $column->format = $format;
    $column->decimal = $decimal;
    $column->section = $this;
    $column->report = $this->report;

    $this->columns[$name] = $column;
    return $column;
  }

  /**
   * Add a object as a row.
   *
   * @param FastPage_Object $object Object to add as a new row.
   * @return FastPage_LogFormatter_ReportRow A new row object.
   */
  public function add_object($object) {
    $row = new FastPage_LogFormatter_ReportRow;

    // Format values.
    foreach ( $this->columns as $name => $col ) {
      $prop = $col->prop;
      if ( $col->format == 'float' ) {
        $decimal = $col->decimal? $col->decimal: 2;
        $format = "%0.${decimal}f";
        $row->$prop = number_format( $object->$prop, $decimal );
      } else if ( $col->format == 'integer' ) {
        $row->$prop = number_format( $object->$prop );
      } else {
        $row->$prop = $object->$prop? $object->$prop: '';
      }
    }

    $row->column = $col;
    $row->section = $this;
    $row->report = $this->report;
    $this->rows []= $row;
  }

  /**
   * Output formatted section.
   *
   * @return array Output lines.
   */
  public function output( $glue = ' ', $tabulation = true ) {
    $output = array();

    // Fix each column width for tabulation.
    if ( $tabulation ) {
      foreach ( $this->columns as $name => $col ) {
        $prop = $col->prop;
        $length = strlen($name);
        foreach ( $this->rows as $row ) {
          if ( $length < strlen($row->$prop) ) {
            $length = strlen($row->$prop);
          }
        }
        $col->width = $length;
      }
    }

    // Output columns.
    $cols = array();
    foreach ( $this->columns as $name => $col ) {
      if ( $tabulation ) {
        $cols []= str_pad( $col->name, $col->width + 1, ' ', STR_PAD_RIGHT );
      } else {
        $cols []= $col->name;
      }
    }
    $output []= join( $glue, $cols );

    // Output rows.
    foreach ( $this->rows as $row ) {
      $cols = array();
      foreach ( $this->columns as $name => $col ) {
        $prop = $col->prop;
        if ( $tabulation ) {
          $pad_type = $col->format == 'integer' || $col->format == 'float'? STR_PAD_LEFT: STR_PAD_RIGHT;
          $cols []= str_pad( $row->$prop, $col->width + 1, ' ', $pad_type );
        } else {
          $cols []= $row->$prop;
        }
      }
      $output []= join( $glue, $cols );
    }

    return $output;
  }
}

/**
 * Debug log report column.
 *
 * @package FastPage_LogFormatter
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class FastPage_LogFormatter_ReportColumn extends FastPage_Object { }

/**
 * Debug log report row.
 *
 * @package FastPage_LogFormatter
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class FastPage_LogFormatter_ReportRow extends FastPage_Object { }

/**
 * Log formatter plugin.
 *
 * @package FastPage
 * @author Kunihiko Miyanaga <miyanaga@ideamans.com>
 */
class FastPage_Plugin_LogFormatter extends FastPage_Plugin {
  /**
   * Plugin constructor.
   *
   * @param FastPage $fastpage FastPage instance.
   */
  public function __construct( $fastpage ) {
    parent::__construct( $fastpage );
    $this->version = FASTPAGE_LOG_FORMATTER_VERSION;

    // Add callback to output_headers.
    $fastpage->add_callback( 'output_headers', 10, array( $this, 'cb_output_headers' ) );

    // Add callback to app_finish.
    $fastpage->add_callback( 'app_finished', 10, array( $this, 'cb_app_finished' ) );
  }

  /**
   * Create new debug report.
   *
   * @param FastPage $fastpage FastPage instance.
   * @param FastPage_Debug $debug Debug object.
   */
  public function create_report( $fastpage, $debug ) {
    // Create a report.
    $report = new FastPage_LogFormatter_Report;

    // Section: Log messages.
    $logs = $debug->log_records();
    $section = $report->add_section('Log Messages');
    $section->add_column( 'Time(msec)', 'timestamp', 'float', 4 );
    $section->add_column( 'Level', 'level', 'integer' );
    $section->add_column( 'Message', 'message' );
    foreach ( $debug->log_records() as $log ) {
      $section->add_object($log);
    }

    // Section: Profiles.
    $section = $report->add_section('Profiles');
    $section->add_column( 'Process', 'name' );
    $section->add_column( 'Start(msec)', 'timestamp_from', 'float', 4 );
    $section->add_column( 'Duration(msec)', 'delta_timestamp', 'float', 4 );
    $section->add_column( 'Memory Allocated', 'delta_memory_usage', 'integer' );
    $section->add_column( 'Total Memory Usage', 'memory_usage_to', 'integer' );

    // Visualize nesting.
    $nesting = array();
    foreach ( $debug->log_profiles() as $profile ) {
      while ( $parent = array_pop($nesting) ) {
        if ( $parent->timestamp_to >= $profile->timestamp_from ) {
          $nesting []= $parent;
          break;
        }
      }
      $profile->name = str_repeat( '-', count($nesting) ) . $profile->name;
      $nesting []= $profile;
      $section->add_object($profile);
    }

    return $report;
  }

  /**
   * Handler of output_headers callback.
   *
   * Add debug logs to response headers to output.
   *
   * @param FastPage $fastpage FastPage instance.
   * @param string $event Event name.
   * @param FastPage_Context FastPage context instance.
   */
  public function cb_output_headers( $callback, $context ) {
    $fastpage = $callback->fastpage;
    $debug = FastPage_Debug::instance();
    if ( !$debug ) return true;

    $in = $debug->debug_in();

    // Create a report.
    $report = $this->create_report( $fastpage, $debug );
    foreach ( $report->sections as $title => $section ) {
      $title = preg_replace( '!\\s+!', '-', $title );
      $output = $section->output();
      $count = count($output);

      $i = 0;
      foreach ( $output as $line ) {
        $name = 'X-FastPage-' . $title;
        if ( $count > 1 ) {
          $name .= sprintf( '-%04d', $i++ );
        }
        $context->response->headers[$name] = $line;
      }
    }

    $debug->debug_out($in);

    return true;
  }

  /**
   * Handler for app_finised callback.
   */
  public function cb_app_finished( $callback ) {
    $fastpage = $callback->fastpage;
    $context = $fastpage->last_context();
    $debug = FastPage_Debug::instance();
    if ( !$debug || !$context ) return true;
    $in = $debug->debug_in();

    // Supports only for html and css.
    $content_type = $context->response->content_type;
    if ( !preg_match( '!^text/(html|css|javascript)$!', $content_type ) ) {
      return true;
    }

    // Create a report.
    $report = $this->create_report( $fastpage, $debug );

    // Commenting.
    $comment_open = $content_type == 'text/html'? '<!--': '/**';
    $comment_close = $content_type == 'text/html'? '-->': '*/';

    // Report total.
    $total = $report->add_section('Total');
    $total->add_column( '', 'name');
    $total->add_column( 'Duration(msec)', 'duration', 'float', 4 );
    $total->add_column( 'Memory Usage', 'memory_usage', 'integer' );

    // Gross.
    $gross = new FastPage_Object;
    $gross->name = 'Gross';
    $gross->duration = FastPage_Util::millisec() - $debug->start;
    $gross->memory_usage = FastPage_Util::memory_usage();
    $total->add_object($gross);

    // Debugging.
    $debugging = new FastPage_Object;
    $debugging->name = 'Debugging';

    // Debug out here.
    $debug->debug_out($in);

    $debugging->duration = $debug->debugging_time;
    $debugging->memory_usage = $debug->debugging_memory_usage;
    $total->add_object($debugging);

    // Net.
    $net = new FastPage_Object;
    $net->name = 'Net';
    $net->duration = $gross->duration - $debugging->duration;
    $net->memory_usage = $gross->memory_usage - $debugging->memory_usage;
    $total->add_object($net);

    // Ouput each section.
    ob_start();
    echo "\n\n$comment_open\n\n";
    foreach ( $report->sections as $title => $section ) {
      echo "= $title =\n\n";
      echo join( "\n", $section->output() );
      echo "\n\n";
    }
    echo $comment_close;
    ob_end_flush();
  }
}
