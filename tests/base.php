<?php

function cb_global_function_string($callback, $args, $hint) {
  $result = join(' ', array( get_class($callback->fastpage), $callback->event, $args['arg'], $hint['hint'] ));
  return $result;
}

class FastPage_BaseTest extends UnitTestCase {
  public function testAccessor() {
    $fp = new FastPage();

    // String.
    $fp->string = 'STRING';
    $this->assertEqual( $fp->string, 'STRING' );

    // Number.
    $fp->numeric = 1;
    $this->assertEqual( $fp->numeric, 1 );
  }

  public function testDictionary() {
    $fp = new FastPage();

    // Add value.
    $value = $fp->__dictionary( 'dic1', 'key1', 'VALUE1' );
    $this->assertEqual( $value, 'VALUE1' );
    $value = $fp->__dictionary( 'dic1', 'key1' );
    $this->assertEqual( $value, 'VALUE1' );

    // Change value.
    $fp->__dictionary( 'dic1', 'key1', 'VALUE2' );
    $value = $fp->__dictionary( 'dic1', 'key1' );
    $this->assertEqual( $value, 'VALUE2' );

    // As raw array.
    $fp->__dictionary( 'dic1', 'key2', 'VALUE2' );
    $raw = $fp->dic1;
    $this->assertTrue( is_array($raw) );
    $this->assertEqual( count($raw), 2 );

    // Other namespace.
    $fp->__dictionary( 'dic1', 'key1', 'dic1-VALUE1' );
    $fp->__dictionary( 'dic2', 'key1', 'dic2-VALUE1' );
    $dic1value = $fp->__dictionary( 'dic1', 'key1' );
    $dic2value = $fp->__dictionary( 'dic2', 'key1' );
    $this->assertEqual( $dic1value, 'dic1-VALUE1' );
    $this->assertEqual( $dic2value, 'dic2-VALUE1' );
  }

  public function cb_object_function_string( $callback, $args, $hint ) {
    return cb_global_function_string( $callback, $args, $hint );
  }

  public static function cb_class_function_string( $callback, $args, $hint ) {
    return cb_global_function_string( $callback, $args, $hint );
  }

  public function testCallbackTypes() {
    $fp = new FastPage();

    // Callback global function.
    $fp->add_callback('event1', 5, 'cb_global_function_string', array('hint' => 'HINT1'));
    $result = $fp->run_callbacks('event1', FastPage_Callback::first_one, array('arg' => 'ARG1'));
    $this->assertEqual( $result, 'FastPage event1 ARG1 HINT1' );

    // Callback object method.
    $fp->add_callback('event2', 5, array($this, 'cb_object_function_string'), array('hint' => 'HINT2'));
    $result = $fp->run_callbacks('event2', FastPage_Callback::first_one, array('arg' => 'ARG2'));
    $this->assertEqual( $result, 'FastPage event2 ARG2 HINT2' );

    // Callback static method.
    /* For 5.2.3+
    $fp->add_callback('event3', 5, 'self::cb_class_function_string', array('hint' => 'HINT3'));
    $result = $fp->run_callbacks('event3', FastPage_Callback::first_one, array('arg' => 'ARG3'));
    $this->assertEqual( $result, 'FastPage event3 ARG3 HINT3' );
    */

    // Callback create function.
    $func = create_function('$callback, $args, $hint',
      'return cb_global_function_string( $callback, $args, $hint );'
    );
    $fp->add_callback('event4', 5, $func, array('hint' => 'HINT4'));
    $result = $fp->run_callbacks('event4', FastPage_Callback::first_one, array('arg' => 'ARG4'));
    $this->assertEqual( $result, 'FastPage event4 ARG4 HINT4' );
  }

  public function testCallbackPriority() {
    $fp = new FastPage();

    $arguments = '$callback, $args, $hint';

    // First.
    $func = create_function($arguments, 'return 1;');
    $fp->add_callback('event1', 5, $func);
    $r = $fp->run_callbacks('event1', FastPage_Callback::first_one);
    $this->assertEqual( $r, 1 );

    // Next, esult will not be changed.
    $func = create_function($arguments, 'return 2;');
    $fp->add_callback('event1', 6, $func);
    $r = $fp->run_callbacks('event1', FastPage_Callback::first_one);
    $this->assertEqual( $r, 1 );

    // Next, esult will be changed.
    $func = create_function($arguments, 'return 3;');
    $fp->add_callback('event1', 4, $func);
    $r = $fp->run_callbacks('event1', FastPage_Callback::first_one);
    $this->assertEqual( $r, 3 );
  }

  public function testCallbackAsSet() {
    $fp = new FastPage();

    $arguments = '$callback, $args, $hint';

    // First.
    $func = create_function($arguments, 'return true;');
    $fp->add_callback('event1', 1, $func);

    // Second.
    $func = create_function($arguments, 'return false;');
    $fp->add_callback('event1', 2, $func);

    // Set.
    $r = $fp->run_callbacks('event1', FastPage_Callback::results_array);
    $this->assertTrue($r[0]);
    $this->assertFalse($r[1]);

    // And.
    $r = $fp->run_callbacks('event1', FastPage_Callback::results_and);
    $this->assertFalse($r);

    // Or.
    $r = $fp->run_callbacks('event1', FastPage_Callback::results_or);
    $this->assertTrue($r);

  }
}
