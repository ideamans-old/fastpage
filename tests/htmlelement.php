<?php

class FastPage_HTMLElementTest extends UnitTestCase {
  protected $img_html = array(
    'double_quoted' => array(
      'original'  => '<img src="test.jpg" class="CLASS1 CLASS2" style="width:80%;">',
      'changed'   => '<img src="changed.gif" class="CLASS1 CLASS2" style="width:80%;">',
      'removed'   => '<img class="CLASS1 CLASS2" style="width:80%;">',
    ),
    'single_quoted' => array(
      'original'  => "<img src='test.jpg' class='CLASS1 CLASS2' style='width:80%;'>",
      'changed'   => "<img src='changed.gif' class='CLASS1 CLASS2' style='width:80%;'>",
      'removed'   => "<img class='CLASS1 CLASS2' style='width:80%;'>",
    ),
    'merged_quoted' => array(
      'original'  => '<img src=\'test.jpg\' class="CLASS1 CLASS2" style="width:80%;">',
      'changed'   => '<img src=\'changed.gif\' class="CLASS1 CLASS2" style="width:80%;">',
      'removed'   => '<img class="CLASS1 CLASS2" style="width:80%;">',
    ),
    'complex' => array(
      'original'  => "<img\nsrc='test.jpg'\tclass='CLASS1 CLASS2' style='width:80%;'>",
      'changed'   => "<img\nsrc='changed.gif'\tclass='CLASS1 CLASS2' style='width:80%;'>",
      'removed'   => "<img\nclass='CLASS1 CLASS2' style='width:80%;'>",
    ),
  );

  public function testParseErrlr() {
    $fp = new FastPage();

    $el = $fp->parse_html_element('img src="test.jpg"');
    $this->assertFalse(isset($el));

    $el = $fp->parse_html_element('<img src="test.jpg"');
    $this->assertFalse(isset($el));

    $el = $fp->parse_html_element('img src="test.jpg">');
    $this->assertFalse(isset($el));

    $el = $fp->parse_html_element('<img src="test.jpg">');
    $this->assertIsA($el, 'FastPage_HTMLelement');
  }

  public function testGetValues() {
    $fp = new FastPage();

    foreach ( $this->img_html as $html ) {
      $el = $fp->parse_html_element($html['original']);

      // Tag name.
      $this->assertEqual($el->tag_name, 'img');

      // An attribute.
      $src = $el->attribute('src');
      $this->assertEqual($src, 'test.jpg');
      $class = $el->attribute('class');
      $this->assertEqual($class, 'CLASS1 CLASS2');

      // All attributes.
      $attrs = $el->attributes();
      $this->assertEqual($attrs['src'], 'test.jpg');
      $this->assertEqual($attrs['class'], 'CLASS1 CLASS2');
    }

  }

  public function testSetValues() {
    $fp = new FastPage();

    foreach ( $this->img_html as $html ) {
      $el = $fp->parse_html_element($html['original']);

      // Change src attribute.
      $el->attribute('src', 'changed.gif');
      $this->assertEqual($el->html, $html['changed']);

      // Get changed attribute.
      $src = $el->attribute('src');
      $this->assertEqual($src, 'changed.gif');
    }
  }

  public function testRemove() {
    $fp = new FastPage();

    foreach ( $this->img_html as $html ) {
      $el = $fp->parse_html_element($html['original']);

      // Remove src attribute.
      $el->remove_attribute('src');
      $this->assertEqual($el->html, $html['removed']);

      // Test cache.
      $this->assertFalse(array_key_exists('src',$el->_attribute ));
      $this->assertTrue(is_null($el->_attributes));
    }
  }

  public function testCaching() {
    $fp = new FastPage();

    foreach ( $this->img_html as $html ) {
      $el = $fp->parse_html_element($html['original']);

      // Not cached yet.
      $this->assertEqual(count($el->_attribute), 0);
      $this->assertNull($el->_attributes);

      // Src attribute.
      $src = $el->attribute('src');
      $this->assertEqual($el->_attribute['src'], 'test.jpg');
      $this->assertFalse(isset($el->_attribute['class']));

      // Change the attribute.
      $el->attribute('src', 'changed.gif');
      $this->assertEqual($el->_attribute['src'], 'changed.gif');
      $this->assertFalse(isset($el->_attribute['class']));

      // All attributes.
      $attrs = $el->attributes();
      $this->assertTrue(is_array($el->_attributes));
      $this->assertEqual($el->_attributes['src'], 'changed.gif');
      $this->assertEqual($el->_attributes['class'], 'CLASS1 CLASS2');

      // Change again.
      $el->attribute('src', 'changed-again.gif');
      $this->assertFalse(isset($el->_attributes));

    }
  }
}
