<?php

// Use simpletest library.
require_once('simpletest/unit_tester.php');
require_once('simpletest/autorun.php');

// Load FastPage
require_once('../fastpage/lib/fastpage.php');

// Create test suite.
$suite = new TestSuite();
$suite->TestSuite('FastPage Tests');

// Load and add test cases.
$dh = opendir('.');
while ( false !== ($file = readdir($dh)) ) {
  if( substr( $file, -4, 4 ) == '.php' && $file != 'index.php' ) {
    $suite->addFile($file);
  }
}

// Run the test suite.
$suite->run(new HtmlReporter());

