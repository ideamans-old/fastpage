<?php

class FastPage_PathTest extends UnitTestCase {
  public function testURLtoPath() {
    $fp = new FastPage_App();

    $fp->config()->default_host = $host = 'www.example.com';
    $fp->config()->default_document_root = $root = '/var/www/htdocs';

    // Root file.
    $this->assertEqual( $fp->url_to_path('/index.html'), "$root/index.html" );

    // With params.
    $this->assertEqual( $fp->url_to_path('/index.html?v=1'), "$root/index.html" );
    $this->assertEqual( $fp->url_to_path('/index.html#v=1'), "$root/index.html" );

    // Subdir file.
    $this->assertEqual( $fp->url_to_path('/css/base.css'), "$root/css/base.css" );

    // Duplicated separator.
    $this->assertEqual( $fp->url_to_path('/css///base.css'), "$root/css/base.css" );

    // Same domain.
    $this->assertEqual( $fp->url_to_path('http://www.example.com/path/to/index.html'), "$root/path/to/index.html" );
    $this->assertEqual( $fp->url_to_path('https://www.example.com/path/to/index.html'), "$root/path/to/index.html" );
    $this->assertEqual( $fp->url_to_path('//www.example.com/path/to/index.html'), "$root/path/to/index.html" );

    // Another domain.
    $this->assertNull( $fp->url_to_path('http://example.com/path/to/index.html') );

    // Additional domain.
    $altroot = '/var/www/vhost/';
    $fp->config()->document_root( 'www.vhost.com', '/var/www/vhost/' );
    $this->assertEqual( $fp->url_to_path('http://www.vhost.com/path/to/index.html'),
      $altroot . 'path/to/index.html'
    );

    // Unset domain.
    $this->assertNull( $fp->url_to_path('http://example.com/path/to/index.html') );

    // Relative URL.
    $this->assertEqual( $fp->url_to_path('to/index.html', "$root/path"), "$root/path/to/index.html" );
    $this->assertEqual( $fp->url_to_path('../to/index.html', "$root/path"), "$root/to/index.html" );
    $this->assertEqual( $fp->url_to_path('path/../to/index.html', "$root"), "$root/to/index.html" );
  }
}
