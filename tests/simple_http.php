<?php

class FastPage_SimpleHTTPTest extends UnitTestCase {
  public function testGet() {
    $fp = new FastPage_App();

    $url_base = $_SERVER['SCRIPT_URI'];
    $url_base = preg_replace( '!/[^/]*$!', '/simplehttp/', $url_base );
    $http = $fp->simple_http();

    foreach ( array( 'builtin', 'curl', 'http_request' ) as $lib ) {
      $fp->config('http')->library = $lib;
      $result = $http->get( $url_base . 'return_ok.php' );
      $this->assertEqual( $result, 'OK' );
      $this->assertEqual( $http->library, $lib );

      $result = $http->get( $url_base . 'accept_encoding.php' );
      $this->assertEqual( $result, 'OK' );
    }
  }
}
